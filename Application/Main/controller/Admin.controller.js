sap.ui.define([
    'jquery.sap.global',
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/Filter',
    'sap/ui/model/json/JSONModel',
    'sap/base/Log'
], function (jQuery, MessageToast, Fragment, Controller, Filter, JSONModel, Log) {
    "use strict";
    var pID;
    var base;
    var CController = Controller.extend("SapUI5Tutorial.Application.Main.controller.Admin", {

        onInit: function () {
            base = this;
            this.checkOnline();
            this.adminProject();

            if (localStorage.kullaniciAdi == 1) {

                this.getView().byId("cikisYap").setVisible(true);
            }
            else if (localStorage.kullaniciAdi == 2) {
                window.location.href = "#/Takvim";
            }
            else {
                window.location.href = "";
            }
            this.getSplitAppObj().setHomeIcon({
                'phone': 'phone-icon.png',
                'tablet': 'tablet-icon.png',
                'icon': 'desktop.ico'
            });

            this.colorsPicker();
        },

        projeKaydet: function () {
            this.checkOnline();
            if (!(localStorage.kullaniciAdi == 1)) {
                sap.m.MessageToast.show("Yönetici olmadan proje ekleyemezsiniz!")
            }
            else {
                if (pID) {
                    var projeAdi = this.getView().byId("projeAdi").getValue();
                    var baslangicTarihi = this.getView().byId("baslangicTarihi").getValue();
                    var bitisTarihi = this.getView().byId("bitisTarihi").getValue();
                    var tip = this.getView().byId("tip").getSelectedItem().getText();

                    databaseHandler.projeUpdate(projeAdi, baslangicTarihi, bitisTarihi, tip, pID).then(function (results) {
                        base.adminProject();
                    }).catch(function (error) {
                        console.log(error);
                    });

                }
                else {

                    //Flag kontrolü burada olacak
                    var projeAdi = this.getView().byId("projeAdi").getValue();
                    var baslangicTarihi = this.getView().byId("baslangicTarihi").getValue();
                    var bitisTarihi = this.getView().byId("bitisTarihi").getValue();
                    var tip = this.getView().byId("tip").getSelectedItem().getText();

                    databaseHandler.projeEkleme(localStorage.kullaniciAdi, projeAdi.toString(), baslangicTarihi.toString(), bitisTarihi.toString(), tip.toString()).then(function (results) {

                    }).catch(function (error) {
                        console.log(error);
                    });
                }
                this.getView().byId("projeAdi").setValue("");
                this.getView().byId("bitisTarihi").setValue("");
                this.getView().byId("baslangicTarihi").setValue("");
                this.getView().byId("tip").setValue("");
                pID = "";
            }
            base.adminProject();
        },

        colorsPicker: function () {
            this.checkOnline();
            var dizi = ["Type01", "Type02", "Type03", "Type04", "Type05", "Type06", "Type07", "Type08", "Type09", "Type10", "Type11", "Type12", "Type13",
                "Type14", "Type15", "Type16", "Type17", "Type18", "Type19", "Type20"]
            var dizi2 = [];
            var id = 0;
            dizi.forEach(item => {
                dizi2.push({
                    "id": id,
                    "renk": item
                })
                id++;
            })
            oModel.setProperty("/ColorList", dizi2);
        },

        logout: function () {
            localStorage.clear();
            window.location.href = "";
        },

        onOrientationChange: function (oEvent) {
            this.checkOnline();
            var bLandscapeOrientation = oEvent.getParameter("landscape"),
                sMsg = "Orientation now is: " + (bLandscapeOrientation ? "Landscape" : "Portrait");
            MessageToast.show(sMsg, { duration: 5000 });
        },

        onPressNavToDetail: function (oEvent) {
            this.checkOnline();
            this.getSplitAppObj().to(this.createId("detailDetail"));
        },

        onPressDetailBack: function () {
            this.checkOnline();
            this.getSplitAppObj().backDetail();
        },

        onPressMasterBack: function () {
            this.checkOnline();
            this.getSplitAppObj().backMaster();
        },

        onPressGoToMaster: function () {
            this.checkOnline();
            this.getSplitAppObj().toMaster(this.createId("master2"));
        },

        onListItemPress: function (oEvent) {
            this.checkOnline();
            var sToPageId = oEvent.getParameter("listItem").getCustomData()[0].getValue();

            this.getSplitAppObj().toDetail(this.createId(sToPageId));
        },

        onPressModeBtn: function (oEvent) {
            this.checkOnline();
            var sSplitAppMode = oEvent.getSource().getSelectedButton().getCustomData()[0].getValue();

            this.getSplitAppObj().setMode(sSplitAppMode);
            MessageToast.show("Split Container mode is changed to: " + sSplitAppMode, { duration: 5000 });
        },

        getSplitAppObj: function () {
            this.checkOnline();
            var result = this.byId("SplitAppDemo");
            if (!result) {
                Log.info("SplitApp object can't be found");
            }
            return result;
        },

        adminProject: function () {
            this.checkOnline();
            databaseHandler.getMultipleProject().then(function (results) {
                var data = Object.assign([], results.rows);
                var dizi = [];
                data.forEach(item => {
                    var obj = {
                        projeID: item.projeID,
                        kullaniciID: item.kullaniciID,
                        projeAdi: item.projeAdi,
                        projeBaslangicTarihi: item.projeBaslangicTarihi,
                        projeBitisTarihi: item.projeBitisTarihi,
                        tip: item.tip
                    }

                    dizi.push(obj);

                });
                oModel.setProperty("/adminProjectModel", dizi);

            }).catch(function (error) {
                console.log(error);
            });
        },

        adminProjectClick: function (oEvent) {
            this.checkOnline();
            console.log(oModel.getProperty(oEvent.oSource.getBindingContextPath()));

            var pAdi = oModel.getProperty(oEvent.oSource.getBindingContextPath()).projeAdi;
            var pBaslangic = oModel.getProperty(oEvent.oSource.getBindingContextPath()).projeBaslangicTarihi;
            var pBitis = oModel.getProperty(oEvent.oSource.getBindingContextPath()).projeBitisTarihi;
            var pTip = oModel.getProperty(oEvent.oSource.getBindingContextPath()).tip;
            pID = oModel.getProperty(oEvent.oSource.getBindingContextPath()).projeID;
            this.getView().byId("projeAdi").setValue(pAdi);
            this.getView().byId("baslangicTarihi").setValue(pBaslangic);
            this.getView().byId("bitisTarihi").setValue(pBitis);
            this.getView().byId("tip").setValue(pTip);
        },

        newProjectFunction: function () {
            this.getView().byId("projeAdi").setValue("");
            this.getView().byId("baslangicTarihi").setValue("");
            this.getView().byId("bitisTarihi").setValue("");
            this.getView().byId("tip").setValue("Type01");
            pID = "";
        },

        deleteProjectFunction: function () {
            databaseHandler.projeSilme(pID).then(function (results) {
                base.adminProject();
            }).catch(function (error) {
                console.log(error);
            });
            this.getView().byId("projeAdi").setValue("");
            this.getView().byId("baslangicTarihi").setValue("");
            this.getView().byId("bitisTarihi").setValue("");
            this.getView().byId("tip").setValue("Type01");
        },

        checkOnline: function () {
            var saat = new Date();
            var suankiSaat = saat.getTime();
            var sonuc = (parseInt(localStorage.cikmasiGerekenSaat) - suankiSaat);
            if (sonuc < 0) {
                sonuc = sonuc * (-1);
            }

            if (sonuc > 3600000 || localStorage.cikmasiGerekenSaat == undefined) {
                window.location.href = "";
                localStorage.clear();
            }
        },

    });

    return CController;

});