// var db1 = {
//     db: null,
//     createPersonel: function () {
//         this.db = window.openDatabase(
//             "ToDo",
//             "1.0",
//             "ToDo Database",
//             1000000);
//         this.db.transaction(
//             function (tx) {
//                 tx.executeSql("create table if not exists personelTable (kullaniciID INTEGER PRIMARY KEY, empMail VARCHAR(50), empPassword VARCHAR(15) )",
//                     [],
//                     function (tx, results) { },
//                     function (tx, error) {
//                         console.log("Error while creating the table: " + error.message);
//                         debugger
//                     }
//                 );
//             },
//             function (error) {
//                 console.log("Transaction error: " + error.message);
//             },
//             function () {
//                 console.log("Create DB transaction completed successfully");
//             }
//         );
//     },
// }


var db = openDatabase('Takvim', '1.0', 'Takvim Uygulaması', 2 * 1024 * 1024);

db.transaction(function (tx) {

    tx.executeSql('CREATE TABLE IF NOT EXISTS Kullanicilar (kullaniciID INTEGER PRIMARY KEY, kullaniciMail VARCHAR(20), kullaniciSifre VARCHAR(20), kullaniciRol VARCHAR (5))');
    // tx.executeSql('INSERT INTO Kullanicilar (kullaniciMail,kullaniciSifre,kullaniciRol) VALUES ("admin","123",1)');
    // tx.executeSql('INSERT INTO Kullanicilar (kullaniciMail,kullaniciSifre,kullaniciRol) VALUES ("ahmet@gmail.com","123",2)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS Projeler (kullaniciID INTEGER, projeID INTEGER PRIMARY KEY, projeAdi VARCHAR(50), projeBaslangicTarihi VARCHAR(20), projeBitisTarihi VARCHAR(20), tip VARCHAR(20))');
    tx.executeSql('CREATE TABLE IF NOT EXISTS Planlar (kullaniciID INTEGER, planID INTEGER PRIMARY KEY, planAdi VARCHAR(50), planBaslangicTarihi VARCHAR(30), planBitisTarihi VARCHAR(30), planTip VARCHAR (20))');
});

var databaseHandler = {
    girisKontrolu: function (kullaniciMail, kullaniciSifre) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("SELECT * FROM Kullanicilar WHERE kullaniciMail=?  AND kullaniciSifre=?", [kullaniciMail, kullaniciSifre], function (transaction, results) {
                    resolve(results);
                },
                    function (error) {
                        reject(error);
                    });
            });
        });
    },

    projeEkleme: function (kullaniciID, projeAdi, projeBaslangicTarihi, projeBitisTarihi, tip) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("INSERT INTO Projeler (kullaniciID, projeAdi, projeBaslangicTarihi, projeBitisTarihi, tip) VALUES (?,?,?,?,?)", [kullaniciID, projeAdi, projeBaslangicTarihi, projeBitisTarihi, tip], function (results) {
                    resolve(results);
                }, function (error) {
                    reject(error);
                });
            });
        });
    },

    planEkleme: function (kullaniciID, planAdi, planBaslangicTarihi, planBitisTarihi, planTip) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("INSERT INTO Planlar (kullaniciID, planAdi, planBaslangicTarihi, planBitisTarihi, planTip) VALUES (?,?,?,?,?)", [kullaniciID, planAdi, planBaslangicTarihi, planBitisTarihi, planTip], function (results) {
                    resolve(results);
                }, function (error) {
                    reject(error);
                });
            });
        });
    },

    planCekme: function () {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("SELECT * FROM Planlar", [], function (transaction, results) {
                    resolve(results);
                },
                    function (error) {
                        reject(error);
                    });
            });
        });
    },

    getSingleProject: function (projeID) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("SELECT * FROM Projeler WHERE projeID=?", [projeID], function (transaction, results) {
                    resolve(results);
                },
                    function (error) {
                        reject(error);
                    });
            });
        });
    },

    getMultipleProject: function () {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("SELECT * FROM Projeler", [], function (transaction, results) {
                    resolve(results);
                },
                    function (error) {
                        reject(error);
                    });
            });
        });
    },

    planSilme: function (planID) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql('DELETE FROM Planlar WHERE planID=?', [planID], function (results) {
                    resolve(results);
                }, function (error) {
                    reject(error);
                })
            })
        })
    },

    planUpdate: function (planAdi, planBaslangicTarihi, planBitisTarihi, planTip, planID) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql('UPDATE Planlar SET planAdi=?, planBaslangicTarihi=?, planBitisTarihi=?, planTip=? WHERE planID=?',
                [planAdi, planBaslangicTarihi, planBitisTarihi, planTip, planID], function (results) {
                    resolve(results);
                }, function (error) {
                    reject(error);
                })
            })
        })
    },

    projeUpdate: function (projeAdi, projeBaslangicTarihi, projeBitisTarihi, tip, projeID ) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql('UPDATE Projeler SET projeAdi=?, projeBaslangicTarihi=?, projeBitisTarihi=?, tip=? WHERE projeID=?',
                [projeAdi, projeBaslangicTarihi, projeBitisTarihi, tip, projeID], function (results) {
                    resolve(results);
                }, function (error) {
                    reject(error);
                })
            })
        })
    },

    projeSilme: function (projeID) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql('DELETE FROM Projeler WHERE projeID=?', [projeID], function (results) {
                    resolve(results);
                }, function (error) {
                    reject(error);
                })
            })
        })
    },

    live: function (projeAdi) {

        return new Promise(function (resolve, reject) {

            db.transaction(function (tx) {

                tx.executeSql("SELECT * FROM Projeler WHERE projeAdi LIKE '%"+projeAdi+"%'", [], function (transaction, results) {
                    resolve(results);
                },
                    function (error) {
                        reject(error);
                    });
            });
        });
    },
}
