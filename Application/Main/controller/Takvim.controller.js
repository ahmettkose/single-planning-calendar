sap.ui.define([
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/format/DateFormat",
	"sap/ui/model/json/JSONModel",
	"sap/ui/unified/library",
	"sap/m/MessageToast",
	"sap/ui/unified/CalendarDayType",
	"sap/ui/core/CalendarType",
	'sap/m/Token',
	'sap/m/SinglePlanningCalendarView',

],
	function (Fragment, Controller, DateFormat, JSONModel, unifiedLibrary, MessageToast, _Token) {
		"use strict";
		var base;
		var globalModel;
		var array = [];
		var globalpID;
		//var CalendarDayType = unifiedLibrary.CalendarDayType;
		var CalendarDayType = sap.ui.unified.CalendarDayType;
		return Controller.extend("SapUI5Tutorial.Application.Main.controller.Takvim", {

			onInit: function () {
				this.checkOnline();
				if (localStorage.kullaniciAdi == 1 || localStorage.kullaniciAdi == 2) {
					this.getView().byId("userLogoutButton").setVisible(true);
				}

				if (localStorage.kullaniciAdi == undefined) {
					sap.m.MessageToast.show("Lütfen önce giriş yapınız...");
					window.location.href = "";
				}

				var oModel = new JSONModel();
				base = this;
				base.getView().byId("SPC1").removeAllAppointments();

				this.planlariGetir();
				oModel = new JSONModel();
				oModel.setData({ stickyMode: "None", enableAppointmentsDragAndDrop: true, enableAppointmentsResize: true, enableAppointmentsCreate: true });
				this.getView().setModel(oModel, "settings");

				oModel = new JSONModel();
				oModel.setData({ allDay: false });
				this.getView().setModel(oModel, "allDay");


			},

			handleAppointmentDrop: function (oEvent) {
				this.checkOnline();
				var oAppointment = oEvent.getParameter("appointment"),
					oStartDate = oEvent.getParameter("startDate"),
					oEndDate = oEvent.getParameter("endDate"),
					bCopy = oEvent.getParameter("copy"),
					sAppointmentTitle = oAppointment.getTitle(),
					oModel = this.getView().getModel(),
					oNewAppointment;

				if (bCopy) {
					oNewAppointment = {
						title: sAppointmentTitle,
						icon: oAppointment.getIcon(),
						text: oAppointment.getText(),
						type: oAppointment.getType(),
						startDate: oStartDate,
						endDate: oEndDate
					};
					oModel.getData().appointments.push(oNewAppointment);
					oModel.updateBindings();
				} else {
					oAppointment.setStartDate(oStartDate);
					oAppointment.setEndDate(oEndDate);

					var a = oEvent.mParameters.appointment.sId
					var b = a.split("-");
					var y = parseInt(b[4]);

					var pID = array[y].pID;
					databaseHandler.planUpdate(sAppointmentTitle, oStartDate.toString(), oEndDate.toString(), oAppointment.mProperties.type, pID).then(function (results) {
					}).catch(function (error) {
						console.log(error);
					});
				}

			},

			handleAppointmentResize: function (oEvent) {
				this.checkOnline();
				var oAppointment = oEvent.getParameter("appointment"),
					oStartDate = oEvent.getParameter("startDate"),
					oEndDate = oEvent.getParameter("endDate"),
					sAppointmentTitle = oAppointment.getTitle();

				oAppointment.setStartDate(oStartDate);
				oAppointment.setEndDate(oEndDate);

				var a = oEvent.mParameters.appointment.sId
				var b = a.split("-");
				var y = parseInt(b[4]);

				var pID = array[y].pID;
				databaseHandler.planUpdate(sAppointmentTitle, oStartDate.toString(), oEndDate.toString(), oAppointment.mProperties.type, pID).then(function (results) {
				}).catch(function (error) {
					console.log(error);
				});
			},

			handleAppointmentCreateDnD: function (oEvent) {
				this.checkOnline();
				var oStartDate = oEvent.getParameter("startDate"),
					oEndDate = oEvent.getParameter("endDate"),
					sAppointmentTitle = "Yeni Proje",
					oModel = this.getView().getModel(),
					oNewAppointment = {
						title: sAppointmentTitle,
						startDate: oStartDate,
						endDate: oEndDate
					};

				oModel.getData().appointments.push(oNewAppointment);
				oModel.updateBindings();

			},

			onExit: function () {
				this.checkOnline();
				this.sPath = null;
				if (this._oDetailsPopover) {
					this._oDetailsPopover.destroy();
				}
				if (this._oNewAppointmentDialog) {
					this._oNewAppointmentDialog.destroy();
				}
			},

			handleAppointmentSelect: function (oEvent) {
				this.checkOnline();
				var oAppointment = oEvent.getParameter("appointment"),
					oStartDate,
					oEndDate,
					oTrimmedStartDate,
					oTrimmedEndDate,
					bAllDate,
					oModel;

				if (oAppointment === undefined) {
					return;
				}

				oStartDate = oAppointment.getStartDate();
				oEndDate = oAppointment.getEndDate();
				oTrimmedStartDate = new Date(oStartDate);
				oTrimmedEndDate = new Date(oEndDate);
				bAllDate = false;
				oModel = this.getView().getModel("allDay");

				if (!oAppointment.getSelected()) {
					this._oDetailsPopover.close();
					return;
				}

				this._setHoursToZero(oTrimmedStartDate);
				this._setHoursToZero(oTrimmedEndDate);

				if (oStartDate.getTime() === oTrimmedStartDate.getTime() && oEndDate.getTime() === oTrimmedEndDate.getTime()) {
					bAllDate = true;
				}

				oModel.getData().allDay = bAllDate;
				oModel.updateBindings();

				if (!this._oDetailsPopover) {
					Fragment.load({
						id: "popoverFrag",
						name: "SapUI5Tutorial.Application.Main.fragment.Details",
						controller: this
					})
						.then(function (oPopoverContent) {
							this._oDetailsPopover = oPopoverContent;
							this._oDetailsPopover.setBindingContext(oAppointment.getBindingContext());
							this.getView().addDependent(this._oDetailsPopover);
							this._oDetailsPopover.openBy(oAppointment);
						}.bind(this));
				} else {
					this._oDetailsPopover.setBindingContext(oAppointment.getBindingContext());
					this._oDetailsPopover.openBy(oAppointment);
				}

				var a = oEvent.mParameters.appointments[0].sId
				var b = a.split("-");
				globalpID = parseInt(b[4]);
				globalpID = array[globalpID].pID;
			},

			handleMoreLinkPress: function (oEvent) {
				this.checkOnline();
				var oDate = oEvent.getParameter("date"),
					oSinglePlanningCalendar = this.getView().byId("SPC1");

				oSinglePlanningCalendar.setSelectedView(oSinglePlanningCalendar.getViews()[2]); // DayView

				this.getView().getModel().setData({ startDate: oDate }, true);
			},

			handleEditButton: function (oEvent) {
				this.checkOnline();
				// The sap.m.Popover has to be closed before the sap.m.Dialog gets opened
				this._oDetailsPopover.close();
				this.sPath = this._oDetailsPopover.getBindingContext().getPath();
				this._arrangeDialogFragment("Plan Düzenle");
			},

			handlePopoverDeleteButton: function () {
				this.checkOnline();
				var oModel = this.getView().getModel(),
					oAppointments = oModel.getData().appointments,
					oAppointment = this._oDetailsPopover._getBindingContext().getObject();

				this._oDetailsPopover.close();

				oAppointments.splice(oAppointments.indexOf(oAppointment), 1);
				oModel.updateBindings();
				var pID = globalpID;
				console.log("pid " + pID);

				databaseHandler.planSilme(pID).then(function (results) {
					console.log(results);
				}).catch(function (error) {
					console.log(error);
				});
			},

			_arrangeDialogFragment: function (sTitle) {
				this.checkOnline();
				if (!this._oNewAppointmentDialog) {
					Fragment.load({
						id: "dialogFrag",
						name: "SapUI5Tutorial.Application.Main.fragment.Modify",
						controller: this
					})
						.then(function (oDialog) {
							this._oNewAppointmentDialog = oDialog;
							this.getView().addDependent(this._oNewAppointmentDialog);
							this._arrangeDialog(sTitle);
						}.bind(this));
				} else {
					this._arrangeDialog(sTitle);
				}
			},

			_arrangeDialog: function (sTitle) {
				this.checkOnline();
				this._setValuesToDialogContent();
				this._oNewAppointmentDialog.setTitle(sTitle);
				// this.singleProject();
				this._oNewAppointmentDialog.open();
			},

			_setValuesToDialogContent: function (oEvent) {
				this.checkOnline();
				var bAllDayAppointment = (Fragment.byId("dialogFrag", "allDay")).getSelected(),
					sStartDatePickerID = bAllDayAppointment ? "DPStartDate" : "DTPStartDate",
					sEndDatePickerID = bAllDayAppointment ? "DPEndDate" : "DTPEndDate",
					oTitleControl = Fragment.byId("dialogFrag", "appTitle"),
					// oTextControl = Fragment.byId("dialogFrag", "moreInfo"),
					oTypeControl = Fragment.byId("dialogFrag", "appType"),
					oStartDateControl = Fragment.byId("dialogFrag", sStartDatePickerID),
					oEndDateControl = Fragment.byId("dialogFrag", sEndDatePickerID),
					oContext,
					oContextObject,
					oSPCStartDate,
					sTitle,
					// sText,
					oStartDate,
					oEndDate,
					sType;
				if (this.sPath) {
					oContext = this._oDetailsPopover.getBindingContext();
					oContextObject = oContext.getObject();
					sTitle = oContextObject.title;
					// sText = oContextObject.text;
					oStartDate = oContextObject.startDate;
					oEndDate = oContextObject.endDate;
					sType = oContextObject.type;
				} else {
					sTitle = "";
					// sText = "";
					oSPCStartDate = this.getView().byId("SPC1").getStartDate();
					oStartDate = new Date(oSPCStartDate);
					oEndDate = new Date(globalModel);
					sType = "Type01";
				}
				if (sTitle == "") {
					oTitleControl.setValue(sTitle);
				} else {
					var z = new sap.m.Token({
						"key": sType,
						"text": sTitle
					})
					oTitleControl.setTokens([z]);
				}

				// oTextControl.setValue(sText);
				oStartDateControl.setDateValue(oStartDate);
				oEndDateControl.setDateValue(oEndDate);
				oTypeControl.setSelectedKey(sType);
			},

			handleDialogOkButton: function (oEvent) {
				this.checkOnline();
				var bAllDayAppointment = (Fragment.byId("dialogFrag", "allDay")).getSelected(),
					sStartDate = bAllDayAppointment ? "DPStartDate" : "DTPStartDate",
					sEndDate = bAllDayAppointment ? "DPEndDate" : "DTPEndDate",
					sTitle = Fragment.byId("dialogFrag", "appTitle").getTokens()[0].getText(),
					sKey = Fragment.byId("dialogFrag", "appTitle").getTokens()[0].getKey(),
					// sText = Fragment.byId("dialogFrag", "moreInfo").getTokens(),
					sType = Fragment.byId("dialogFrag", "appType").getSelectedItem().getText(),
					oStartDate = Fragment.byId("dialogFrag", sStartDate).getDateValue(),
					oEndDate = Fragment.byId("dialogFrag", sEndDate).getDateValue(),
					oModel = this.getView().getModel(),
					sAppointmentPath;
				// sap.ui.getCore().byId("appTitle").removeAllTokens();
				Fragment.byId("dialogFrag", "appTitle").removeAllTokens();

				if (localStorage.kullaniciAdi == undefined) {
					sap.m.MessageToast.show("Giriş yapmadan kayıt yapılamaz!");
					this._oNewAppointmentDialog.close();
				}
				else {

					if (Fragment.byId("dialogFrag", sStartDate).getValueState() !== "Error"
						&& Fragment.byId("dialogFrag", sEndDate).getValueState() !== "Error") {

						if (this.sPath) {
							sAppointmentPath = this.sPath;
							oModel.setProperty(sAppointmentPath + "/title", sTitle);
							// oModel.setProperty(sAppointmentPath + "/text", sText);
							oModel.setProperty(sAppointmentPath + "/type", sType);
							oModel.setProperty(sAppointmentPath + "/startDate", oStartDate);
							oModel.setProperty(sAppointmentPath + "/endDate", oEndDate);

							databaseHandler.planUpdate(sTitle, oStartDate.toString(), oEndDate.toString(), sKey, globalpID).then(function (results) {
								base.planlariGetir();
							}).catch(function (error) {
								console.log(error);
							});
						} else {
							// oModel.getData().appointments.push({
							// 	title: sTitle,
							// 	// text: sText,
							// 	type: sType,
							// 	startDate: oStartDate,
							// 	endDate: oEndDate
							// });

							databaseHandler.planEkleme(localStorage.kullaniciAdi, sTitle.toString(), oStartDate, oEndDate, sKey).then(function (results) {
								base.planlariGetir();
							}).catch(function (error) {
								console.log(error);
							});
						}

						oModel.updateBindings();

						this._oNewAppointmentDialog.close();
					}
				}
			},

			formatDate: function (oDate) {
				this.checkOnline();
				if (oDate) {
					var iHours = oDate.getHours(),
						iMinutes = oDate.getMinutes(),
						iSeconds = oDate.getSeconds();

					if (iHours !== 0 || iMinutes !== 0 || iSeconds !== 0) {
						return DateFormat.getDateTimeInstance({ style: "medium" }).format(oDate);
					} else {
						return DateFormat.getDateInstance({ style: "medium" }).format(oDate);
					}
				}
			},

			handleDialogCancelButton: function () {
				this.checkOnline();
				this.sPath = null;
				Fragment.byId("dialogFrag", "appTitle").removeAllTokens();
				this._oNewAppointmentDialog.close();
			},

			handleCheckBoxSelect: function (oEvent) {
				this.checkOnline();
				var bSelected = oEvent.getSource().getSelected(),
					sStartDatePickerID = bSelected ? "DTPStartDate" : "DPStartDate",
					sEndDatePickerID = bSelected ? "DTPEndDate" : "DPEndDate",
					oOldStartDate = Fragment.byId("dialogFrag", sStartDatePickerID).getDateValue(),
					oNewStartDate = new Date(oOldStartDate),
					oOldEndDate = Fragment.byId("dialogFrag", sEndDatePickerID).getDateValue(),
					oNewEndDate = new Date(oOldEndDate);

				if (!bSelected) {
					oNewStartDate.setHours(this._getDefaultAppointmentStartHour());
					oNewEndDate.setHours(this._getDefaultAppointmentEndHour());
				} else {
					this._setHoursToZero(oNewStartDate);
					this._setHoursToZero(oNewEndDate);
				}

				sStartDatePickerID = !bSelected ? "DTPStartDate" : "DPStartDate";
				sEndDatePickerID = !bSelected ? "DTPEndDate" : "DPEndDate";
				Fragment.byId("dialogFrag", sStartDatePickerID).setDateValue(oNewStartDate);
				Fragment.byId("dialogFrag", sEndDatePickerID).setDateValue(oNewEndDate);
			},

			_getDefaultAppointmentStartHour: function () {
				return 9;
			},

			_getDefaultAppointmentEndHour: function () {
				return 18;
			},

			_setHoursToZero: function (oDate) {
				oDate.setHours(0, 0, 0, 0);
			},

			userLogout: function () {
				localStorage.clear();
				window.location.href = "";
			},

			aylikTiklama: function (oEvent) {
				this.checkOnline();
				this._createInitialDialogValues(this.getView().byId("SPC1"));
				this.getView().byId("SPC1").mProperties.startDate = oEvent.mParameters.startDate.setHours(9);
				globalModel = oEvent.mParameters.endDate - 6 * 60 * 60 * 1000;
				this._createInitialDialogValues(this.getView().byId("SPC1").getStartDate());
			},

			handleAppointmentCreate: function (oEvent) {
				this.checkOnline();
				this.getView().byId("SPC1").mProperties.startDate = oEvent.getParameters().startDate;
				var bitis = oEvent.getParameters().endDate;
				globalModel = bitis;
				this._createInitialDialogValues(this.getView().byId("SPC1").getStartDate());
			},

			// handleHeaderDateSelect: function (oEvent) {
			// 	this._createInitialDialogValues(oEvent.getParameter("date"));
			// },

			_createInitialDialogValues: function (oDate) {
				this.checkOnline();
				this.sPath = null;
				this._arrangeDialogFragment("Plan Oluştur");
			},

			handleStartDateChange: function (oEvent) {
				this.checkOnline();
				var oStartDate = oEvent.oSource.getStartDate()
			},

			updateButtonEnabledState: function (oDateTimePickerStart, oDateTimePickerEnd, oButton) {
				this.checkOnline();
				var bEnabled = oDateTimePickerStart.getValueState() !== "Error"
					&& oDateTimePickerStart.getValue() !== ""
					&& oDateTimePickerEnd.getValue() !== ""
					&& oDateTimePickerEnd.getValueState() !== "Error";

				oButton.setEnabled(bEnabled);
			},

			handleDateTimePickerChange: function () {
				this.checkOnline();
				var oDateTimePickerStart = Fragment.byId("dialogFrag", "DTPStartDate"),
					oDateTimePickerEnd = Fragment.byId("dialogFrag", "DTPEndDate"),
					oStartDate = oDateTimePickerStart.getDateValue(),
					oEndDate = oDateTimePickerEnd.getDateValue(),
					bEndDateBiggerThanStartDate = oEndDate.getTime() <= oStartDate.getTime(),
					bErrorState = oStartDate && oEndDate && bEndDateBiggerThanStartDate;

				this._setDateValueState(oDateTimePickerStart, bErrorState);
				this._setDateValueState(oDateTimePickerEnd, bErrorState);
				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, this._oNewAppointmentDialog.getBeginButton());
			},

			handleDatePickerChange: function () {
				this.checkOnline();
				var oDatePickerStart = Fragment.byId("dialogFrag", "DPStartDate"),
					oDatePickerEnd = Fragment.byId("dialogFrag", "DPEndDate"),
					oStartDate = oDatePickerStart.getDateValue(),
					oEndDate = oDatePickerEnd.getDateValue(),
					bEndDateBiggerThanStartDate = oEndDate.getTime() <= oStartDate.getTime(),
					bErrorState = oStartDate && oEndDate && bEndDateBiggerThanStartDate;

				this._setDateValueState(oDatePickerStart, bErrorState);
				this._setDateValueState(oDatePickerEnd, bErrorState);
				this.updateButtonEnabledState(oDatePickerStart, oDatePickerEnd, this._oNewAppointmentDialog.getBeginButton());
			},

			_setDateValueState: function (oPicker, bErrorState) {
				this.checkOnline();
				var sValueStateText = "Start date should be before End date";

				if (bErrorState) {
					oPicker.setValueState("Error");
					oPicker.setValueStateText(sValueStateText);
				} else {
					oPicker.setValueState("None");
				}
			},

			handleOpenLegend: function (oEvent) {
				this.checkOnline();
				var oSource = oEvent.getSource();

				if (!this._oLegendPopover) {
					Fragment.load({
						id: "LegendFrag",
						name: "SapUI5Tutorial.Application.Main.fragment.Legend",
						controller: this
					}).then(function (oPopoverContent) {
						this._oLegendPopover = oPopoverContent;
						this.getView().addDependent(this._oLegendPopover);
						this._oLegendPopover.openBy(oSource);
					}.bind(this));
				} else if (this._oLegendPopover.isOpen()) {
					this._oLegendPopover.close();
				} else {
					this._oLegendPopover.openBy(oSource);
				}
			},

			planlariGetir: function () {
				this.checkOnline();
				oModel.setData({});
				base.getView().setModel(oModel);

				array = [];
				databaseHandler.planCekme().then(function (results) {
					var arr = Object.assign([], results.rows);

					arr.forEach(function (item) {

						var ap = {
							title: item.planAdi,
							type: item.planTip,
							startDate: new Date(item.planBaslangicTarihi),
							endDate: new Date(item.planBitisTarihi),
							pID: item.planID
						};
						array.push(ap);

					})
					oModel.setData({
						startDate: new Date(),
						types: (function () {
							var aTypes = [];
							for (var key in CalendarDayType) {
								aTypes.push({
									type: CalendarDayType[key]
								});
							}
							return aTypes;
						})(),
						appointments: array,
						legendAppointmentItems: [
							{
								text: "Takım Toplantısı",
								type: CalendarDayType.Type01
							},
							{
								text: "Kişisel",
								type: CalendarDayType.Type05
							},
							{
								text: "Tartışmalar",
								type: CalendarDayType.Type08
							},
							{
								text: "Ofis Dışı",
								type: CalendarDayType.Type09
							},
							{
								text: "Özel Toplantı",
								type: CalendarDayType.Type03
							}
						]
					});

					base.getView().setModel(oModel);
					var x = [];
					x.push(array);
				}).catch(function (error) {
					console.log(error);
				});
			},

			singleProject: function () {
				// var dizi = [];
				// databaseHandler.getMultipleProject().then(function (results) {
				// 	var data = Object.assign([], results.rows);

				// 	data.forEach(item => {
				// 		var obj = {
				// 			projeID: item.projeID,
				// 			kullaniciID: item.kullaniciID,
				// 			projeAdi: item.projeAdi,
				// 			projeBaslangicTarihi: item.projeBaslangicTarihi,
				// 			projeBitisTarihi: item.projeBitisTarihi,
				// 			tip: item.tip
				// 		}
				// 		dizi.push(obj);
				// 	});
				// 	oModel.setProperty("/projectModel", dizi);
				// }).catch(function (error) {
				// 	console.log(error);
				// });
			},

			liveCh: function (oEvent) {
				this.checkOnline();
				var appTitle = sap.ui.getCore().byId("appTitle");
				oEvent.oSource.mBindingInfos.suggestionItems.path = "";
				var dizi = [];
				var newValue = oEvent.getParameter("newValue");
				if (newValue.length > 2) {
					databaseHandler.live(newValue).then(function (results) {
						var data = Object.assign([], results.rows);
						data.forEach(item => {
							var obj = {
								projeID: item.projeID,
								kullaniciID: item.kullaniciID,
								projeAdi: item.projeAdi,
								projeBaslangicTarihi: item.projeBaslangicTarihi,
								projeBitisTarihi: item.projeBitisTarihi,
								tip: item.tip
							}
							dizi.push(obj);
						});
						oModel.setProperty("/projectModel", dizi);
					}).catch(function (error) {
						console.log(error);
					});
				} else {
					oModel.setProperty("/projectModel", []);
				}
			},

			checkOnline: function () {
				var saat = new Date();
				var suankiSaat = saat.getTime();
				if ((parseInt(localStorage.cikmasiGerekenSaat) - suankiSaat) > 3600000 || localStorage.cikmasiGerekenSaat == undefined){
					window.location.href = "";
					localStorage.clear();
				}
			},


		});
	});