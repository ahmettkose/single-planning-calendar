sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/m/MessageBox',
    'sap/ui/model/json/JSONModel'
], function (Controller, MessageBox, JSONModel) {

    'use strict';
    return Controller.extend("SapUI5Tutorial.Application.Main.controller.Login", {


        //     databaseHandler.createPersonel();
        //     // var array = [];

        //     // array.push({
        //     //     "ad": "ahmet",
        //     //     "soyad": "köse",
        //     //     "dgYil": "1995"
        //     // })
        //     // oModel.setProperty("/Bilgiler", array);
        onInit: function (oEvent) {
        },

        login: function () {
            this.checkOnline();
            var that = this;
            var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
            // Element :  var isim = this.getView().byId("kul_adi").getValue();
            // Dialog  :  var isim2 = sap.ui.getCore().byId("kul_adi2").getValue();
            var k_adi = this.getView().byId("kul_adi").getValue();
            var sifre = this.getView().byId("pass").getValue();

            

            databaseHandler.girisKontrolu(k_adi, sifre).then(function (results) {

                // console.log(results.rows[0]);
                var row = results.rows.length;

                if (row > 0) {

                    localStorage.kullaniciAdi = results.rows[0].kullaniciID;


                    if (localStorage.kullaniciAdi == 1) {
                        loRouter.navTo("Admin");
                        window.location.reload(false);
                        sap.m.MessageToast.show("Yönetici Girişi");
                    }

                    else if (localStorage.kullaniciAdi == 2) {
                        loRouter.navTo("Takvim");
                        window.location.reload(false);
                        sap.m.MessageToast.show("Üye Girişi");
                    }

                } else {
                    that.getView().byId("kul_adi").setValue("")
                    that.getView().byId("pass").setValue("")

                    sap.m.MessageBox.error('Kullanıcı adı ve şifrenizi kontrol ediniz', {
                        title: 'Hata',
                        actions: ['Tamam'],
                        onClose: function (oAction) {
                            if (oAction === "Tamam") {

                            }
                        }
                    })
                }
            }).catch(function (error) {
                console.log(error);
            });
        },

        checkOnline: function () {
            var saat = new Date();
            var cikmasiGerekenSaat = saat.setHours(saat.getHours()+1);
            localStorage.setItem('cikmasiGerekenSaat', cikmasiGerekenSaat);
        }
    });
});






























        // getUrunler: function(){
        //     var bosArray = [];
        //     urunGetir().then(function(fulfilled){
        //        var dizi = Object.assign([], fulfilled.rows)
        //         if(dizi){
        //             dizi.forEach(element => {
        //                 bosArray.push({
        //                     "Adi":element.ad,
        //                     "Soyadi":element.soyad,
        //                     "dYili":element.yil,
        //                 })
        //             });
        //             oModel.setProperty("/Bilgilerim", bosArray);
        //         }
        //     })
        // }


